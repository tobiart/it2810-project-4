import React, {useState} from 'react'

//Logikk for åpning og lukking av modal

function Modalhook() {

    const [isOpen, setisOpen] = useState(false);
    const [triggerNumber, settriggerNumber] = useState(-1)

    function toggle(num: number){
      settriggerNumber(num)
      
        
      setisOpen(!isOpen); 
    };
    
  return{
    isOpen,
    toggle,
    triggerNumber
  }


}

export default Modalhook