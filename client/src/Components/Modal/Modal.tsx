
import React, {ReactNode, useEffect, useContext} from 'react'
import getPokemon from '../../Api/queries/pokemon/getPokemon';
import getPokemonList from '../../Api/queries/pokemon/getPokemonList';
import './Modal.css';
import addDownvote from '../../Api/mutations/votes/addDownvote';
import addUpvote from '../../Api/mutations/votes/addUpvote';
import removeDownvote from '../../Api/mutations/votes/removeDownvote';
import removeUpvote from '../../Api/mutations/votes/removeUpvote';
import { useQuery, useMutation} from '@apollo/client';
import addPokemonToTeam from '../../Api/mutations/trainer/addPokemonToTeam';
import removePokemonFromTeam from '../../Api/mutations/trainer/removePokemonFromTeam';
import getTrainer from '../../Api/queries/trainer/getTrainer';
import TrainerContext from '../../Components/TrainerContext';


interface ModalType {
    children?: ReactNode;
    isOpen: boolean;
    pokemonIndex: number;
    toggle: () => void;
}

type pokemon = {
  pokeindex: number,
  name: string,
  img: object,
  type1: string,
  type2: string | null,
  prevevo: [] | null,
  nextevo: [] | null,
  upvotes: number | null,
  downvotes: number | null
}


function upperCase(i: string){  
  return i.charAt(0).toUpperCase() + i.slice(1)
} //Tar inn en string og gjør første bokstav stor

var prevEvolution: number | null = 1;
var nextEvolution: number | null = 1;
var listOfEvos: string[] = [];


function Modal(props: ModalType) {
  

  const {currentTrainer} = useContext(TrainerContext) //context for hvilken trainer som er "logget inn"

  const {loading, data, refetch: refetchMon} = useQuery(getPokemon(["pokeIndex","name","types", "prevEvo", "nextEvo", "img", "upvotes", "downvotes"]), { 
    variables: { pokeIndex: props.pokemonIndex },
  }); //får pokemonIndex fra pokedisplay og henter pokemonen med den indexen


  const {data: trainerData, refetch} = useQuery(getTrainer(["pokeIndex"]), {
    variables: {username: currentTrainer}
  }) //Henter traineren med navnet på currentTrainer

  const {data: prevData, loading:prevLoading} = useQuery(getPokemon(["name"]), {
    variables: { nextEvo: props.pokemonIndex },
  });

  const {data:nextData} = useQuery(getPokemonList(["pokemon{name}"]), {
    variables: { prevEvo: props.pokemonIndex, sortOrder: 1 },
  });


  //Her ligger mutationsene som kan brukes senere. Vi kan legge til eller fjerne pokemon fra team, eller adde eller fjerne upvotes og downvotes
  const [addPokemonMutation] = useMutation(addPokemonToTeam(["pokeIndex", "img", "name"])); 
  const [removePokemonMutation] = useMutation(removePokemonFromTeam(["pokeIndex", "name"]));
  const [addUpvoteMutation] = useMutation(addUpvote(["pokeIndex", "name"]));
  const [addDownvoteMutation] = useMutation(addDownvote(["pokeIndex", "name"]));
  const [removeUpvoteMutation] = useMutation(removeUpvote(["pokeIndex", "name"]))
  const [removeDownvoteMutation] = useMutation(removeDownvote(["pokeIndex", "name"]))


  //Funksjoner for å legge til eller fjerne votes, kjøres på onClick
  function addDownVote(){
    addDownvoteMutation({variables:{pokeIndex: props.pokemonIndex, username: currentTrainer}})
  }
  function addUpVote(){
    addUpvoteMutation({variables:{pokeIndex: props.pokemonIndex, username: currentTrainer}})
  }
  function removeDownVote(){
    removeDownvoteMutation({variables:{pokeIndex: props.pokemonIndex, username: currentTrainer}})
  }
  function removeUpVote(){
    removeUpvoteMutation({variables: {pokeIndex: props.pokemonIndex, username: currentTrainer}})
  }





  if(data && props.pokemonIndex > 0){ //Sjekker om current pokemon har en pre-evolution
    if(data.pokemon.prevEvo){
    prevEvolution = data.pokemon.prevEvo[0];
    }
    else{
      prevEvolution = null
    }
  }

  if(data && props.pokemonIndex > 0){
    if(data.pokemon.nextEvo){
      listOfEvos = []
      
      if(nextData){
        nextData.pokemonList.pokemon.forEach((pokemon:pokemon) => {
          listOfEvos.push(upperCase(pokemon.name))
        })
      }
    }
    if(data.pokemon.nextEvo === null){
      nextEvolution= null;
    }
  }
  

  function prevEvoInfo(){ //Skriver ut info om current pokemon sin previous evolution
    if(data && !data.pokemon.nextEvo && !data.pokemon.prevEvo){
      return (upperCase(data.pokemon.name) + ' has no pre-evolutions or possible further evolutions.')
    }
    if(prevEvolution === null){
      return ('')
    }
    else{
      console.log(prevData, prevLoading);
      
      return(upperCase(data.pokemon.name) + ' evolves from ' + upperCase(prevData.pokemon.name) + '.')
    }
  }

  function nextEvoInfo(){ //Skriver ut info om current pokemon sin(e) next evolution(s)

    if(data && data.pokemon.nextEvo && data.pokemon.nextEvo.length > 1){
      
      const lastEvo = listOfEvos.pop();
      return(upperCase(data.pokemon.name) + ' can evolve into either ' + listOfEvos.join(', ') + ' or ' + lastEvo + '.');
    }
    
    
    if(data && data.pokemon.nextEvo && data.pokemon.nextEvo.length === 1){
      const evo = listOfEvos.pop();
      return(upperCase(data.pokemon.name) + ' can evolve into ' + evo + '.');
    }

    if(nextEvolution === null){
      return ('');
    }
    
  }

  function typingInfo(){ //Skriver ut info om pokemon sin typing
    if(data && data.pokemon.types[0] && data.pokemon.types[1]){
      return(upperCase(data.pokemon.name) + ' is a ' + data.pokemon.types[0] + ' / ' + data.pokemon.types[1] + ' type.')
    }
    else{
      return(upperCase(data.pokemon.name) + ' is a ' + data.pokemon.types[0] + ' type.')
    }
  }

  useEffect(() => { //refresher når noe endres slik at det vises i modalen med en gang og man slipper å åpne og lukke den
    refetch()
    refetchMon()
  }, [refetch, refetchMon])
  
  function addPokemon(){ //Funksjon for å adde pokemon, kjøres ved onClick
      addPokemonMutation({variables: {username: currentTrainer, pokeIndex: props.pokemonIndex}})
      refetch()
  }

  function removePokemon(){ //Funksjon for å fjerne pokemon, kjøres ved onClick
    removePokemonMutation({variables: {username: currentTrainer, pokeIndex: props.pokemonIndex}});
    refetch()
  }

  function getTeam(){ //Funksjon for å hente ut teamet til currentTrainer. Om en pokemon finnes her vil avgjøre om knappen for å fjerne denne pokemonen fra traineren sitt team skal rendres.
    var team = [];
    if(trainerData && trainerData.trainer && trainerData.trainer.team && trainerData.trainer.team.length > 0){
      for(var i=0; i < trainerData.trainer.team.length; i++){
        team.push(trainerData.trainer.team[i].pokeIndex)
      }
    }
    return team;
  }

  function getUpvotes(){ //Funksjon for å hente upvotes. Avgjør hvorvidt knappen med tommel opp skal adde eller fjerne upvote.
    var upvotes = [];
    if(trainerData && trainerData.trainer && trainerData.trainer.upvotes.length > 0){
      for(var i = 0; i < trainerData.trainer.upvotes.length; i++){
        upvotes.push(trainerData.trainer.upvotes[i].pokeIndex)
      }
    }
    return upvotes;
  }

  function getDownvotes(){ //Funksjon for å hente downvoets. Avgjør hvorvidt knappen med tommel ned skal adde eller fjerne downvote
    var downvotes = [];
    if(trainerData && trainerData.trainer && trainerData.trainer.downvotes.length > 0){
      for(var i = 0; i < trainerData.trainer.downvotes.length; i++){
        downvotes.push(trainerData.trainer.downvotes[i].pokeIndex)
      }
    }
    return downvotes;
  }


  //Liste over farger som tilhører hver pokemon-type
  const colors = [
    {poketype: 'normal', color: '#A8A77A'},
    {poketype: 'fire', color: '#EE8130'},
    {poketype: 'water', color: '#6390F0'},
    {poketype: 'grass', color: '#7AC74C'},
    {poketype: 'electric', color: '#F7D02C'},
    {poketype: 'ice', color: '#96D9D6'},
    {poketype: 'fighting', color: '#C22E28'},
    {poketype: 'poison', color: '#A33EA1'},
    {poketype: 'ground', color: '#E2BF65'},
    {poketype: 'flying', color: '#A98FF3'},
    {poketype: 'psychic', color: '#F95587'},
    {poketype: 'bug', color: '#A6B91A'},
    {poketype: 'rock', color: '#B6A136'},
    {poketype: 'ghost', color: '#735797'},
    {poketype: 'dragon', color: '#6F35FC'},
    {poketype: 'dark', color: '#705746'},
    {poketype: 'steel', color: '#B7B7CE'},
    {poketype: 'fairy', color: '#D685AD'}
]
//Finner fargen til tilsvarer pokemon til type1
const found1 = colors.find(obj => {
  if(data && data.pokemon){

  return obj.poketype === data.pokemon.types[0]
  }
  else {return null;}
});  
//Finner fargen som tilsvarer pokemon til type2. Hvis type2 er null så blir den satt til fargen til type1.  
const found2 = colors.find(obj => {
  if(data && data.pokemon){
  if(data.pokemon.types.length === 1){
      return obj.poketype === data.pokemon.types[0];
  }
  return obj.poketype === data.pokemon.types[1]}
  else {return null;}
}); 


  return (
    <>
    {/*Dette er for at modalen ikke skal vises før de nødvendige dataene er på plass!*/}
    {loading ? <div></div> : 
    props.isOpen && prevData && nextData && trainerData && found1 && found2 && 
    (
    <div className ="modaloverlay" onClick={props.toggle}>
      <div onClick={(e) => e.stopPropagation()} className="modalbox">
      
      
      <div className="header" style={{ backgroundColor: found1!.color}}>
        <div id ="pokemonName">
        <p>#{data.pokemon.pokeIndex} {upperCase(data.pokemon.name)}</p>
        </div>
        {currentTrainer !== '' && <div className='voteButtons'>
          {((trainerData && trainerData.trainer && trainerData.trainer.upvotes && getUpvotes().indexOf(props.pokemonIndex) === -1) || getUpvotes().length === 0) && <img id="upvoteButton" src="./images/thumb-up-icon.png" onClick={addUpVote} alt='upvote.png'></img>}
          {trainerData && trainerData.trainer && trainerData.trainer.upvotes && getUpvotes().indexOf(props.pokemonIndex) > -1 && <img style={{border:'solid 2px', borderRadius:'36px', boxShadow:'2px 2px'}} id="upvoteButton" src="./images/thumb-up-icon.png" onClick={removeUpVote} alt='upvote.png'></img>}
          {((trainerData && trainerData.trainer && trainerData.trainer.downvotes && getDownvotes().indexOf(props.pokemonIndex) === -1) || getDownvotes().length === 0) && <img id="downvoteButton" src="./images/thumb-down-icon.png" onClick={addDownVote} alt='downvote.png'></img>}
          {trainerData && trainerData.trainer && trainerData.trainer.downvotes && getDownvotes().indexOf(props.pokemonIndex) > -1 && <img style={{border:'solid 2px', borderRadius:'36px', boxShadow:'2px 2px'}} id ="downvoteButton" src="./images/thumb-down-icon.png" onClick={removeDownVote} alt='downvote.png'></img>}
        </div>}
      </div>
       
        {/*Div for info om pokemonen*/}
        <div className="info">
        <img className="bilde" src={'data:image/png;base64,'+data.pokemon.img} alt={data.pokemon.name}></img>
        <div className='evoInfo'>
        <p>{typingInfo()}</p>
        <p>{prevEvoInfo()}</p>
        <p>{nextEvoInfo()}</p>
        <p>It has {data.pokemon.upvotes} upvotes and {data.pokemon.downvotes} downvotes.</p>
        </div>
        </div>
       
        <div className="footer" style={{ backgroundColor: found2!.color}}>
        {trainerData.trainer && trainerData.trainer.team.length < 6 && <button className="addremovebutton" id="addToTeam" onClick = {addPokemon}>Add to team</button>}
         {getTeam().indexOf(props.pokemonIndex) > -1 && <button className="addremovebutton" id ="removeFromTeam" onClick={removePokemon}>Remove from team</button>}
        </div>
      </div>
    </div>
    )}

    </>
  );
}

export default Modal;
