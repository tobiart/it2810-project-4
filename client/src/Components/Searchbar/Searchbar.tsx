import React, {useContext, useState} from 'react';
import './Searchbar.css';
import { default as SearchContext } from '../SearchContext';




function Searchbar(){

    const [navn, setNavn] = useState('');

    const {word, setWord, searchBy, type1, type2, setType1, setsearchBy, setSort, setOrder, setType2} = useContext(SearchContext)


    //Funksjoner som håndterer søk-input

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setNavn(event.target.value);
      };

    const handleClick = (event: React.MouseEvent<HTMLButtonElement> | null) => {
        setWord(navn)
    }

    const handleSortChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        setSort(event.target.value);
    }

    const handleOrderChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        setOrder(parseInt(event.target.value));
    }

    const handleClear = (event: React.MouseEvent<HTMLButtonElement>) => {
        setWord('');
        setType1('');
        setType2('');
        var name = document.getElementById('searchField') as HTMLInputElement;
        var type1select = document.getElementById('type1select') as HTMLSelectElement;
        type1select.selectedIndex = 0;
        name.value='';
    }
    
    return(
        <>

        <div className="headWrapper">
            <div className='overskrift'>
                <h1>Pokédex (by group 60)</h1>
            </div>
            <div className='searchbar' data-testid="searchbar">
                <div className="sokefelt">
                <input
                    id="searchField"
                    type="text"
                    name="message"
                    placeholder="Name"
                    data-testid="input"
                    onChange={handleChange}
                    onKeyDown={event => {
                        if(event.keyCode === 13){
                            handleClick(null);
                        }
                    }}
                />
            <button type="submit" onClick = {handleClick} id ="searchbutton">Search</button>
            <button type="submit" id="clearbutton" onClick={handleClear}>Clear search</button>
            </div>

            <div className="types">
            <select name="type1" id="type1select" defaultValue={undefined} onChange={(event) => 
                {
                    setType1(event.target.value)
                    if (event.target.value === '') {
                        setType2('')
                    }
                }}>
                <option value={undefined} disabled hidden>Select a type</option>
                <option value={''}>Any</option>
                <option value={'normal'}>Normal</option>
                <option value={'fire'}>Fire</option>
                <option value={'water'}>Water</option>
                <option value={'electric'}>Electric</option>
                <option value={'grass'}>Grass</option>
                <option value={'ice'}>Ice</option>
                <option value={'fighting'}>Fighting</option>
                <option value={'poison'}>Poison</option>
                <option value={'ground'}>Ground</option>
                <option value={'flying'}>Flying</option>
                <option value={'psychic'}>Psychic</option>
                <option value={'bug'}>Bug</option>
                <option value={'rock'}>Rock</option>
                <option value={'ghost'}>Ghost</option>
                <option value={'dragon'}>Dragon</option>
                <option value={'dark'}>Dark</option>
                <option value={'steel'}>Steel</option>
                <option value={'fairy'}>Fairy</option>
            </select>

            {                
                type1 && (
                    <select name="type2" id="type2select" defaultValue={undefined} onChange={(event) =>
                        {
                            setType2(event.target.value)
                        }}>
                        <option value={undefined} disabled hidden>Select a type</option>
                        <option value={''}>Any</option>
                        <option value={'normal'}>Normal</option>
                        <option value={'fire'}>Fire</option>
                        <option value={'water'}>Water</option>
                        <option value={'electric'}>Electric</option>
                        <option value={'grass'}>Grass</option>
                        <option value={'ice'}>Ice</option>
                        <option value={'fighting'}>Fighting</option>
                        <option value={'poison'}>Poison</option>
                        <option value={'ground'}>Ground</option>
                        <option value={'flying'}>Flying</option>
                        <option value={'psychic'}>Psychic</option>
                        <option value={'bug'}>Bug</option>
                        <option value={'rock'}>Rock</option>
                        <option value={'ghost'}>Ghost</option>
                        <option value={'dragon'}>Dragon</option>
                        <option value={'dark'}>Dark</option>
                        <option value={'steel'}>Steel</option>
                        <option value={'fairy'}>Fairy</option>
                    </select>
                )
            }
            </div>



            <div className="sortfelt">
            Sort by:
            <select name="sort" id="sortselect" onChange={handleSortChange}>
                <option value="pokeIndex">Index</option>
                <option value="name">Name</option>
                <option value="upvotes">Upvotes</option>
                <option value="downvotes">Downvotes</option>
            </select>
            <select name="order" id="orderselect" onChange={handleOrderChange}>
                <option value="1">Ascending</option>
                <option value="-1">Descending</option>
            </select>
            </div>


        </div>
        </div>
        </>
    );
}

export default Searchbar