import { createContext, SetStateAction, useState } from "react";

type TrainerContextType = {
    currentTrainer: string | null,
    setcurrentTrainer: React.Dispatch<React.SetStateAction<string|null>>
}

const iTrainerContextState = {
   currentTrainer: '',
   setcurrentTrainer: () => {}
}

const TrainerContext = createContext<TrainerContextType>(iTrainerContextState);

export default TrainerContext;