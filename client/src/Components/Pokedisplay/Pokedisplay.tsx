import React, {useEffect, useState, useContext} from 'react';
import './Pokedisplay.css';
import Modal from '../Modal/Modal';
import Modalhook from '../Modal/Modalhook';
import getPokemonList from '../../Api/queries/pokemon/getPokemonList';
import { useQuery} from '@apollo/client';
import { default as SearchContext } from '../SearchContext';



type pokemon = {
    pokeIndex: number,
    name: string,
    img: object,
    types: string[],
    prevEvo: [] | null,
    nextEvo: [] | null
}


function upperCase(i: string){  
    return i.charAt(0).toUpperCase() + i.slice(1)
  } //Funksjon som gjør første bokstaven i en string stor


export default function Pokedisplay(){
    

    const {word, sort, order, type1, type2, setWord, setSort, setOrder} = useContext(SearchContext) //Henter ting som kan påvirke søket fra Searchcontext
    const [page, setPage] = useState(1);
    const {isOpen, toggle, triggerNumber} = Modalhook();


    useEffect(() =>{
        if(word===null){
            setWord("");
        }
        if(word!==null){
            setPage(1);
        }
        if(sort === null){
            setSort("pokeIndex");
        }
        if(order === null){
            setOrder(1);
        }
    }, [setWord, word, setSort, sort, setOrder, order])
    
    const {loading , error, data, previousData} = useQuery(getPokemonList(["pokemon{pokeIndex, name, types, img}, count"]), {
        variables: { pagenr: page, sortBy: sort, sortOrder: order, name: word!, type1: type1!, type2: type2!},
    }); //Henter listen med pokemon som skal vises

    
    const colors = [ //pokemon typene med tilhørende farger
        {poketype: 'normal', color: '#A8A77A'},
        {poketype: 'fire', color: '#EE8130'},
        {poketype: 'water', color: '#6390F0'},
        {poketype: 'grass', color: '#7AC74C'},
        {poketype: 'electric', color: '#F7D02C'},
        {poketype: 'ice', color: '#96D9D6'},
        {poketype: 'fighting', color: '#C22E28'},
        {poketype: 'poison', color: '#A33EA1'},
        {poketype: 'ground', color: '#E2BF65'},
        {poketype: 'flying', color: '#A98FF3'},
        {poketype: 'psychic', color: '#F95587'},
        {poketype: 'bug', color: '#A6B91A'},
        {poketype: 'rock', color: '#B6A136'},
        {poketype: 'ghost', color: '#735797'},
        {poketype: 'dragon', color: '#6F35FC'},
        {poketype: 'dark', color: '#705746'},
        {poketype: 'steel', color: '#B7B7CE'},
        {poketype: 'fairy', color: '#D685AD'}
    ]

    // console.log(data);
    


    return(
        <>
        <div className="pokedisplay" data-testid="pokedisplay">
        <Modal isOpen={isOpen} toggle={()=>toggle(-1)} pokemonIndex={triggerNumber}></Modal>
        {loading ? <div id="loading"><p>Loading</p><img id="loadingball" src='./images/loadingball.gif' alt="loadingball.gif"></img></div> : null}
        {error ? <p>Error :(</p> : null}
        {error ? console.log(error) : null}
        {
        data &&
        data.pokemonList.pokemon.map((pokemon:pokemon) => {
            
            //Finner fargene som tilsvarer pokemonens type(s)
            const found1 = colors.find(obj => {
                return obj.poketype === pokemon.types[0]
            });    
            
            const found2 = colors.find(obj => {
                if(pokemon.types.length === 1){
                    return obj.poketype === pokemon.types[0];
                }
                return obj.poketype === pokemon.types[1]
            });        
            

            return(
                <div onClick={ () => toggle(pokemon.pokeIndex)} className="box" key={pokemon.pokeIndex} style={{ background: 'linear-gradient(149deg, ' + found1!.color+ ' 50%, '+ found2!.color + ' 50%)'}}>
                    <p id ="pokeNumber"> # {pokemon.pokeIndex}</p>
                    <p id ="pokeName">{upperCase(pokemon.name)}</p>
                    <img id ="pokeimg" src={'data:image/png;base64,' + pokemon.img} alt={pokemon.name + '.png'}></img>
                </div>
            )
        })}
           
            
        </div>
        <div className='nextprevbuttons'>
            <button id="prev" onClick={() => { 
                if (page > 1) {
                    setPage(page-1);
                } 
            }
            }>Previous page</button>
            <div id="pageinfo">
            <span className='firstPage' onClick={
                () => {
                    setPage(1);
                }
            }>1</span>&emsp;
            <span className='currentPage'><b>{page}</b></span>&emsp;
            <span className='lastPage' onClick={
                () => {
                    data&&setPage(Math.ceil(data.pokemonList.count/50));
                }
            }>{
            data ? Math.ceil(data.pokemonList.count/50) : previousData ? Math.ceil(previousData.pokemonList.count/50) : 1
            }</span>&emsp;
            </div>
            <button id="next" data-testid="next" onClick={() => {
                console.log("Hello1");
                console.log(data);
                
                
                if (data && (page < data.pokemonList.count/50)){
                    console.log("Hello2");
                    setPage(page+1);
                }
            }}>Next page</button>
        </div>
        </>
    );
}
 

