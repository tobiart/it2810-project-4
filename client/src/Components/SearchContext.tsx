import React, { createContext, SetStateAction, useState } from "react";

type SearchContextType = {
    word: string | null,
    sort: string | null,
    order: number | null,
    searchBy: string | null,
    type1: string | null,
    type2: string | null,
    setWord: React.Dispatch<React.SetStateAction<string|null>>,
    setSort: React.Dispatch<React.SetStateAction<string|null>>,
    setOrder: React.Dispatch<React.SetStateAction<number|null>>,
    setsearchBy: React.Dispatch<React.SetStateAction<string|null>>,
    setType1: React.Dispatch<React.SetStateAction<string|null>>,
    setType2: React.Dispatch<SetStateAction<string | null>>
}

const iSearchContextState = {
    word: '',
    sort: 'pokeIndex',
    order: 1,
    searchBy: 'Name',
    type1: '',
    type2: '',
    setWord: () => {},
    setSort: () => {},
    setOrder: () => {},
    setsearchBy: () => {},
    setType1: () => {},
    setType2: () => {}
}

const SearchContext = createContext<SearchContextType>(iSearchContextState);

export default SearchContext;