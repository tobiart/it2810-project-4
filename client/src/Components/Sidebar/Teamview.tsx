import React, {useContext, useEffect} from 'react'
import {useQuery} from '@apollo/client';
import './Sidebar.css';
import TrainerContext from '../TrainerContext';
import getTrainer from '../../Api/queries/trainer/getTrainer';
import getPokemon from '../../Api/queries/pokemon/getPokemon';




type pokemon = { 
    pokeIndex: number,
    name: string,
    img: object,
    types: string[],
    prevevo: [] | null,
    nextevo: [] | null
}



function Teamview() {


    const {currentTrainer} = useContext(TrainerContext);

    const {data: trainerData, refetch} = useQuery(getTrainer(["pokeIndex", "name", "img"]), {
        variables: {username: currentTrainer},
    });

    const {data: pokeData} = useQuery(getPokemon(["pokeIndex", "name", "img"]), {
      variables: {pokeIndex: 1},
  });

    useEffect(() => { //Refetcher når noe skjer
      refetch()
    }, [currentTrainer, trainerData, refetch, pokeData])


    


  return (
    <>
   
   
        <div className="team">
        {trainerData === undefined ? <p>Loading</p>: null}

        {trainerData && trainerData.trainer && trainerData.trainer.team && trainerData.trainer.team.length === 0 ? <p style={{wordBreak:'break-word', textAlign:'center'}}>Click a pokemon!</p>: null}
        
        {trainerData && trainerData.trainer && trainerData.trainer.team &&
        
        trainerData.trainer.team.map((pokemon:pokemon) => { //Mapper over pokemonene som finnes i traineren sitt team og viser bilder av dem
          return(
            <img className='pokeBilde' src={'data:image/png;base64,' + pokemon.img}></img>
          );
        })
      }
</div>
    </>
  ) 
}
export default Teamview
