import React, {useState, useEffect, useContext} from 'react';
import './Sidebar.css';
import './Teamview';
import getTrainer from '../../Api/queries/trainer/getTrainer';
import addTrainer from '../../Api/mutations/trainer/addTrainer';
import { useMutation, useQuery } from '@apollo/client';
import TrainerContext from '../TrainerContext';
import Teamview from './Teamview';

function Sidebar() {

  const [visible, setVisible] = useState(true);

  const [navn, setNavn] = useState(''); //State som holder styr på hva man skriver inn som navn, hentes ut med onchange / onclick under

  const {currentTrainer, setcurrentTrainer} = useContext(TrainerContext) //context for hvilken trainer som er "logget inn"

  const {data: trainerData, refetch} = useQuery(getTrainer(["pokeIndex", "name", "img"]), {
    variables: { username: currentTrainer }, //Henter ut data til treneren med navnet currentTrainer
  });

  const [addTrainerMutation] = useMutation(addTrainer(["pokeIndex", "name", "img"])) //Mutation for å legge til trener i databasen

  useEffect(() => {
    refetch()
    addTrainerMutation({variables:{username: currentTrainer}})
    if(trainerData){
      addTrainerMutation({variables:{username: currentTrainer}})
      if(trainerData.trainer===null){
        addTrainerMutation({variables:{username: currentTrainer}})
      }
    }
  },[trainerData, refetch, currentTrainer, addTrainerMutation])



  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setNavn(event.target.value); 
  };

  async function handleClick(event: React.MouseEvent<HTMLButtonElement>){

    if(navn.length > 20) {
      alert('Trainer name cannot be over 20 characters long.')
    }
    if(navn.length === 0 ){
      alert('Trainer name cannot be empty.')
    }
    if(navn.length < 20 && navn.length > 0){
      setVisible((prev) => !prev);
      setcurrentTrainer(navn);
    }
  }



  return (
    <div className="sidebar">

      {currentTrainer && 
     
      <div className ="visTrainer">
        {currentTrainer &&  <img id ="trainerIcon" src="./images/trainer-icon.png" alt="trainer.png" />}
     
      <p id ="trainerName">{currentTrainer}</p>

        
      
     
      </div>
}
     
     
     
     
      {visible && !currentTrainer &&
      <div className="trainernameinput">
        <input
        id="trainerNameInput"
        type="text"
        name="message"
        onChange={handleChange}
        data-testid="trainerNameInput"
      />
        <button id ="setTrainerButton" onClick={handleClick}>Choose your trainer name!</button> 

        
     
      </div>}
      {currentTrainer && <div style={{textAlign:'center'}} className="yourTeam"><p style={{wordBreak:'break-word'}}>Your team:</p></div>}
     
       <Teamview/>
    
    </div>
  )
}

export default Sidebar