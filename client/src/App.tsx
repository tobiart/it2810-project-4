import React, {useState} from 'react';
import Searchbar from './Components/Searchbar/Searchbar';
import Pokedisplay from './Components/Pokedisplay/Pokedisplay';
import Sidebar from './Components/Sidebar/Sidebar';
import { default as SearchContext } from './Components/SearchContext';
import { default as TrainerContext} from './Components/TrainerContext';
import './App.css';
import './Fonts/PixeloidSans-nR3g1.ttf';


function App() {


  const [word, setWord] = useState<string | null>(null)
  const [sort, setSort] = useState<string | null>(null)
  const [order, setOrder] = useState<number | null>(null)
  const [searchBy, setsearchBy] = useState<string|null>('Name')
  const [type1, setType1] = useState<string|null>(null);
  const [type2, setType2] = useState<string|null>(null);
  
  const [currentTrainer, setcurrentTrainer] = useState<string | null>('')
  return (
      <>
      <TrainerContext.Provider value = {{currentTrainer, setcurrentTrainer}}>
        <Sidebar/>
        <SearchContext.Provider value={{word, sort, order, searchBy, type1, type2, setWord, setSort, setOrder, setsearchBy, setType1, setType2}}>
          <Searchbar/>
          <Pokedisplay/>
        </SearchContext.Provider>
      </TrainerContext.Provider>
      </>
  );
} 

export default App;
