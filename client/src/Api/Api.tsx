import React from 'react';
import getPokemon from './queries/pokemon/getPokemon';
import getPokemonList from './queries/pokemon/getPokemonList';




// Eksemple på hvordan apiet kan brukes (gjerne kommenter ut flere av eksemplene for å ikke kjøre alle samtidig):
// (Kommenter ut Api(); i App.tsx for å bruke dette eksempelet)
function Api() {
    // --------------------- getPokemon --------------------- //

    // // Henter en pokemon med index 69, og henter ut pokeIndex, name, og typer (man må alltid hente ut noen verdier om pokemonen i en liste):
    // getPokemon(["pokeIndex","name","types"], 69).then((data) => {
    //     if (data === null) {
    //         return
    //     }
    //     console.log(data);
    // });

    // // Henter en pokemon med navn "Pikachu" (må oppgi pokeIndex -1 for å søke etter navn), og henter ut pokeIndex, og evolutions:
    // getPokemon(["pokeIndex","nextEvo"], -1, "pikachu").then((data) => {
    //     if (data === null) {
    //         return
    //     }
    //     console.log(data);
    // });

    // // Henter en pokemon uten no parametere, som vil gi en "haha funny" pokemon, og henter ut navn, upvotes, og downvotes:
    // getPokemon(["name","upvotes","downvotes"]).then((data) => {
    //     if (data === null) {
    //         return
    //     }
    //     console.log(data);
    // });



    // // -------------------------- getPokemonList -------------------------- //

    // // getPokemonList henter ut en liste med pokemoner, og kan filtrere på navn, type1, og type2. 
    // // (Values er dette samme som i getPokemon: verdier for hver pokemon i listen, som må oppgis)
    // // Navn i dette tilfellet er en substring, så i dette tilfellet vil den finne alle navn som inneholder 'b'.
    // getPokemonList(["pokeIndex","name","types"], 1, "b", "grass", "poison").then((data) => {
    //     if (data === null) {
    //         return
    //     }
    //     console.log(data);
    // });

    // //For å hente bare etter type og ikke navn må man gi en tom string til navn.
    // //Og dersom man bare skal ha en type, oppgi bare en, og ikke type1 = "" og type2 = "type".
    // getPokemonList(["pokeIndex","name","types"], 1, "", "poison").then((data) => {
    //     if (data === null) {
    //         return
    //     }
    //     console.log(data);
    // });

    // //Dersom type1 = type2 vil den gi pokemon som har eksusivt den typen.
    // getPokemonList(["pokeIndex","name","types"], 1, "", "poison", "poison").then((data) => {
    //     if (data === null) {
    //         return
    //     }
    //     console.log(data);
    // });

    // //Ingen søkeparametere vil gi en liste med alle pokemoner.
    // getPokemonList(["pokeIndex","name",]).then((data) => {
    //     if (data === null) {
    //         return
    //     }
    //     console.log(data);
    // });


}

export default Api;