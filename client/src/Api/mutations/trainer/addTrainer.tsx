import { gql } from "@apollo/client";

// Must be sent as a mutation
export default function addTrainer(values: String[]) {

    const ADD_TRAINER = gql`
        mutation addTrainer($username: String!) {
            addTrainer(username: $username) {
                username
                team {
                    ${values.join(",")}
                }
                upvotes {
                    ${values.join(",")}
                }
                downvotes {
                    ${values.join(",")}
                }
            }
        }
    `;
    
    // console.log(`
    //     mutation trainer($username: String!) {
    //         trainer(username: $username) {
    //             username
    //             team {
    //                 ${values.join(",")}
    //             }
    //             upvotes {
    //                 ${values.join(",")}
    //             }
    //             downvotes {
    //                 ${values.join(",")}
    //             }
    //         }
    //     }
    // `);
    

    return ADD_TRAINER;
}