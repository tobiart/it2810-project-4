import {gql} from '@apollo/client';

export default function addPokemonToTeam(values: String[]) {

    const ADD_POKEMON_TO_TEAM = gql`
        mutation addPokemonToTeam($username: String!, $pokeIndex: Int!) {
            addPokemonToTeam(username: $username, pokeIndex: $pokeIndex) {
                username
                team {
                    ${values.join(",")}
                }
                upvotes {
                    ${values.join(",")}
                }
                downvotes {
                    ${values.join(",")}
                }
            }
        }
    `;
    
    return ADD_POKEMON_TO_TEAM;
}