import {gql} from '@apollo/client';

export default function removePokemonFromTeam(values: String[]) {

    const REMOVE_POKEMON_FROM_TEAM = gql`
        mutation removePokemonFromTeam($username: String!, $pokeIndex: Int!) {
            removePokemonFromTeam(username: $username, pokeIndex: $pokeIndex) {
                username
                team {
                    ${values.join(",")}
                }
                upvotes {
                    ${values.join(",")}
                }
                downvotes {
                    ${values.join(",")}
                }
            }
        }
    `;
    
    
    return REMOVE_POKEMON_FROM_TEAM;
}