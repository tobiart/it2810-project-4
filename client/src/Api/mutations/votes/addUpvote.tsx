import {gql} from '@apollo/client';

export default function addUpvote(values: String[]) {

    const ADD_UPVOTE = gql`
        mutation addUpvote($pokeIndex: Int!, $username: String!) {
            addUpvote(pokeIndex: $pokeIndex, username: $username) {
                ${values.join(",")}
            }
        }
    `;
    
    return ADD_UPVOTE;

}