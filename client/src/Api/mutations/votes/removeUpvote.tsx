import {gql} from '@apollo/client';

export default function removeUpvote(values: String[]) {

    const REMOVE_UPVOTE = gql`
        mutation removeUpvote($pokeIndex: Int!, $username: String!) {
            removeUpvote(pokeIndex: $pokeIndex, username: $username) {
                ${values.join(",")}
            }
        }
    `;
    
    return REMOVE_UPVOTE;

}