import {gql} from '@apollo/client';

export default function removeDownvote(values: String[]) {

    const REMOVE_DOWNVOTE = gql`
        mutation removeDownvote($pokeIndex: Int!, $username: String!) {
            removeDownvote(pokeIndex: $pokeIndex, username: $username) {
                ${values.join(",")}
            }
        }
    `;
    
    return REMOVE_DOWNVOTE;

}