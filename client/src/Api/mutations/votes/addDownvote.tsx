import {gql} from '@apollo/client';

export default function addDownvote(values: String[]) {

    const ADD_DOWNVOTE = gql`
        mutation addDownvote($pokeIndex: Int!, $username: String!) {
            addDownvote(pokeIndex: $pokeIndex, username: $username) {
                ${values.join(",")}
            }
        }
    `;
    
    return ADD_DOWNVOTE;

}