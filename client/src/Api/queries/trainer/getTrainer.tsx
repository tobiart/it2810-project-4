import { gql } from "@apollo/client";

export default function getTrainer(values: String[]) {

    const GET_TRAINER = gql`
        query trainer($username: String!) {
            trainer(username: $username) {
                username
                team {
                    ${values.join(",")}
                }
                upvotes {
                    ${values.join(",")}
                }
                downvotes {
                    ${values.join(",")}
                }
            }
        }
    `;
    
    return GET_TRAINER;
}