import {gql} from '@apollo/client';

export default function getPokemonList(values: String[]) {

    // var query = "{pokemonList";
    // const valueString = values.join(",");
    // if (pokeName === "" && type1 === "" && type2 === "") {
    //     query += "(pagenr:"+pagenr+",) {"+valueString+"}}";
    // } else {
    //     
    //     query += "(pagenr:"+pagenr+",";
    //     if(prevEvo !== 0) {
    //         query += 'prevEvo:"'+prevEvo+'",';
    //     }
    //     if (pokeName !== "") {
    //         query += 'name:"'+pokeName+'",';
    //     } if (type1 !== "") {
    //         query += 'type1:"'+type1+'",';
    //     } if (type2 !== "") {
    //         query += 'type2:"'+type2+'",';
    //     }
    //     query += "){"+valueString+"}}";
    // }

    const GET_POKEMON_LIST = gql`
        query pokemonList($pagenr: Int, $prevEvo: [Int], $sortBy: String, $sortOrder: Int, $name: String, $type1: String, $type2: String) {
            pokemonList(pagenr: $pagenr, prevEvo: $prevEvo, sortBy: $sortBy, sortOrder: $sortOrder, name: $name, type1: $type1, type2: $type2) {
                ${values.join(",")}
            }
        }
    `;
    // console.log(`
    //     query pokemonList($pagenr: Int, $prevEvo: [Int], $name: String, $type1: String, $type2: String) {
    //         pokemon(pagenr: $pagenr, prevEvo: $prevEvo, name: $name, type1: $type1, type2: $type2, prevEvo: $prevEvo) {
    //             ${values.join(",")}
    //         }
    //     }
    // `
    // );
    
    return GET_POKEMON_LIST;
}