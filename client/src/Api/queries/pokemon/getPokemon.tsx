import { gql } from '@apollo/client';

export default function getPokemon(values: String[]) {

    const GET_POKEMON = gql`
        query pokemon($pokeIndex: Int, $nextEvo: [Int]) {
            pokemon(pokeIndex: $pokeIndex, nextEvo: $nextEvo) {
                ${values.join(",")}
            }
        }
    `;
    
    return GET_POKEMON;
}