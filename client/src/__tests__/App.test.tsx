import React from 'react';
import { render, screen, fireEvent, waitFor} from '@testing-library/react';
import App from '../App';
import { MockedProvider } from '@apollo/client/testing';
import getPokemonList from '../Api/queries/pokemon/getPokemonList';
import getPokemon from '../Api/queries/pokemon/getPokemon';
import addTrainer from '../Api/mutations/trainer/addTrainer';
import { act } from 'react-dom/test-utils';

const mocks = [
    {
        request: {
            query: getPokemonList(["pokemon{pokeIndex, name, types, img}, count"]),
            variables: { pagenr: 1, sortBy: "pokeIndex", sortOrder:1, name: "", type1: null, type2: null},
        },
        result: {
            data: {
                pokemonList: {
                    count: 51,
                    pokemon:[
                        {
                            pokeIndex: 1,
                            name: "Bulbasaur",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/1.png",
                            types: ["grass", "poison"],
                            prevEvo: null
                        },
                        {
                            pokeIndex: 2,
                            name: "Ivysaur",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/2.png",
                            types: ["grass", "poison"],
                            prevEvo: [1]
                        },
                        {
                            pokeIndex: 3,
                            name: "Venusaur",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/3.png",
                            types: ["grass", "poison"],
                            prevEvo: [2]
                        },
                        {
                            pokeIndex: 4,
                            name: "Charmander",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/4.png",
                            types: ["fire"],
                            prevEvo: null
                        },
                        {
                            pokeIndex: 5,
                            name: "Charmeleon",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/5.png",
                            types: ["fire"],
                            prevEvo: [4]
                        },
                        {
                            pokeIndex: 6,
                            name: "Charizard",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/6.png",
                            types: ["fire", "flying"],
                            prevEvo: [5]
                        },
                        {
                            pokeIndex: 7,
                            name: "Squirtle",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/7.png",
                            types: ["water"],
                            prevEvo: null
                        },
                        {
                            pokeIndex: 8,
                            name: "Wartortle",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/8.png",
                            types: ["water"],
                            prevEvo: [7]
                        },
                        {
                            pokeIndex: 9,
                            name: "Blastoise",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/9.png",
                            types: ["water"],
                            prevEvo: [8]
                        },
                        {
                            pokeIndex: 10,
                            name: "Caterpie",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/10.png",
                            types: ["bug"],
                            prevEvo: null
                        },
                        {
                            pokeIndex: 11,
                            name: "Metapod",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/11.png",
                            types: ["bug"],
                            prevEvo: [10]
                        }
                    ]
                }
            },
        },
    },
    {
        request: {
            query: getPokemonList(["pokemon{pokeIndex, name, types, img}, count"]),
            variables: { pagenr: 2, sortBy: "pokeIndex", sortOrder:1, name: "", type1: null, type2: null},
        },
        result: {
            data: {
                pokemonList: {
                    count: 51,
                    pokemon:[
                        {
                            pokeIndex: 12,
                            name: "Butterfree",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/12.png",
                            types: ["bug", "flying"],
                            prevEvo: [11]
                        },
                        {
                            pokeIndex: 13,
                            name: "Weedle",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/13.png",
                            types: ["bug", "poison"],
                            prevEvo: null
                        },
                        {
                            pokeIndex: 14,
                            name: "Kakuna",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/14.png",
                            types: ["bug", "poison"],
                            prevEvo: [13]
                        },
                        {
                            pokeIndex: 15,
                            name: "Beedrill",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/15.png",
                            types: ["bug", "poison"],
                            prevEvo: [14]
                        },
                        {
                            pokeIndex: 16,
                            name: "Pidgey",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/16.png",
                            types: ["normal", "flying"],
                            prevEvo: null
                        }
                    ]
                }
            }
        } 
    },
    {
        request: {
            query: getPokemon(["name", "img", "types"]),
            variables: { pokeIndex: 1},
        },
        result: {
            data: {
                pokemon: {
                    name: "Bulbasaur",
                    img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/1.png",
                    types: ["grass", "poison"],
                }
            },
        }
    },
    {
        request: {
            query: addTrainer(["pokeIndex", "name", "img"]),
            variables: {username:"test"},
        },
        result: {
            data: {
                addTrainer: {
                    username:"test",
                    team:[],
                    upvotes:[],
                    downvotes:[]
                }
            }
        }
    },
    {
        request: {
            query: addTrainer(["pokeIndex", "name", "img"]),
            variables: {username:""},
        },
        result: {
            data: {
                addTrainer: {
                    username:"",
                    team:[],
                    upvotes:[],
                    downvotes:[]
                }
            }
        }
    }
];

// test('renders app', async () => {
//   // act(() => {
//   //   render(
//   //     <MockedProvider mocks={mocks} addTypename={false}>
//   //       <App />
//   //     </MockedProvider>
//   //   );
//   //   });
// });

test('default page pokemon are correct', async () => {
    render(
        <MockedProvider mocks={mocks} addTypename={false}>
            <App />
        </MockedProvider>
    );

    expect(await screen.findByText(/Bulbasaur/)).toBeInTheDocument();
    expect(await screen.findByText(/Ivysaur/)).toBeInTheDocument();
    expect(await screen.findByText(/Venusaur/)).toBeInTheDocument();
    expect(await screen.findByText(/Charmander/)).toBeInTheDocument();
    expect(await screen.findByText(/Charmeleon/)).toBeInTheDocument();
    expect(await screen.findByText(/Charizard/)).toBeInTheDocument();
    expect(await screen.findByText(/Squirtle/)).toBeInTheDocument();
    expect(await screen.findByText(/Wartortle/)).toBeInTheDocument();
    expect(await screen.findByText(/Blastoise/)).toBeInTheDocument();
    expect(await screen.findByText(/Caterpie/)).toBeInTheDocument();
    expect(await screen.findByText(/Metapod/)).toBeInTheDocument();
});

test('page 2 pokemon are correct', async () => {
    render(
        <MockedProvider mocks={mocks} addTypename={false}>
            <App />
        </MockedProvider>
    );
    
    //Checks that the pokemon are not there before the page changes
    expect(screen.queryByText(/Butterfree/)).not.toBeInTheDocument();
    expect(screen.queryByText(/Weedle/)).not.toBeInTheDocument();
    expect(screen.queryByText(/Kakuna/)).not.toBeInTheDocument();
    expect(screen.queryByText(/Beedrill/)).not.toBeInTheDocument();
    expect(screen.queryByText(/Pidgey/)).not.toBeInTheDocument();

    //Wait for the current page to finish loading before changing the page
    await waitFor(() => screen.getByText(/Bulbasaur/));
    act(() => {
        fireEvent.click(screen.getByText(/Next/));
    });
    expect(await screen.findByText(/Butterfree/)).toBeInTheDocument();
    expect(await screen.findByText(/Weedle/)).toBeInTheDocument();
    expect(await screen.findByText(/Kakuna/)).toBeInTheDocument();
    expect(await screen.findByText(/Beedrill/)).toBeInTheDocument();
    expect(await screen.findByText(/Pidgey/)).toBeInTheDocument();
});


test('trainer is added', async () => {
  render(
    <MockedProvider mocks={mocks} addTypename={false}>
      <App />
    </MockedProvider>
  );

  await act(async () => {
    let input = screen.getByTestId("trainerNameInput") as HTMLInputElement;
    fireEvent.change(input, { target: { value: "test" } });
  });
  act(() => {
    fireEvent.click(screen.getByText(/Choose your trainer name!/));
});

  expect(await screen.findByText(/test/)).toBeInTheDocument();
  expect(await screen.findByText(/Your team:/)).toBeInTheDocument();
  
});



    

// test('pokemon is upvoted', async () => {
//   render(
//     <MockedProvider mocks={mocks} addTypename={false}>
//       <App />
//     </MockedProvider>
//   );

//   act(() => {
//     fireEvent.click(screen.getByText(/Add Trainer/));
//   });

//   expect(await screen.findByText(/Username/)).toBeInTheDocument();
//   expect(await screen.findByText(/Team/)).toBeInTheDocument();
//   expect(await screen.findByText(/Upvotes/)).toBeInTheDocument();
//   expect(await screen.findByText(/Downvotes/)).toBeInTheDocument();
// });




