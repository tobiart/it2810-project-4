import "@testing-library/jest-dom"
import { render, screen } from "@testing-library/react"
import { MockedProvider } from "@apollo/client/testing"
import getPokemonList from "../Api/queries/pokemon/getPokemonList"
import Pokedisplay from "../Components/Pokedisplay/Pokedisplay"

const mocks = [
    {
        request: {
            query: getPokemonList(["pokemon{pokeIndex, name, types, img}, count"]),
            variables: {"pagenr":1,"sortBy":"pokeIndex","sortOrder":1,"name":"", "type1": "", "type2": ""}
        },
        result: {
            data: {
                pokemonList: {
                    pokemon: [
                        {
                            pokeIndex: 1,
                            name: "Bulbasaur",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/1.png",
                            types: ["grass", "poison"],
                        },
                        {
                            pokeIndex: 2,
                            name: "Ivysaur",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/2.png",
                            types: ["grass", "poison"],
                        },
                        {
                            pokeIndex: 3,
                            name: "Venusaur",
    
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/3.png",
                            types: ["grass", "poison"],
                        },
                        {
                            pokeIndex: 4,
                            name: "Charmander",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/4.png",
                            types: ["fire"],
                        },
                        {
                            pokeIndex: 5,
                            name: "Charmeleon",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/5.png",
                            types: ["fire"],
                        },
                        {
                            pokeIndex: 6,
                            name: "Charizard",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/6.png",
                            types: ["fire", "flying"],
                        },
                        {
                            pokeIndex: 7,
                            name: "Squirtle",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/7.png",
                            types: ["water"],
                        },
                        {
                            pokeIndex: 8,
                            name: "Wartortle",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/8.png",
                            types: ["water"],
                        },
                        {
                            pokeIndex: 9,
                            name: "Blastoise",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/9.png",
                            types: ["water"],
                        },
                        {
                            pokeIndex: 10,
                            name: "Caterpie",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/10.png",
                            types: ["bug"],
                        }
                    ],
                    count: 10
                }
            }
        }
    }
]


test("renders pokemon list and checks for name", async () => {
    render(
        <MockedProvider mocks={mocks} addTypename={false}>
            <Pokedisplay />
        </MockedProvider>
    )


    expect(await screen.findByText(/Bulbasaur/)).toBeInTheDocument()
    expect(await screen.findByText(/Ivysaur/)).toBeInTheDocument()
    expect(await screen.findByText(/Venusaur/)).toBeInTheDocument()
    expect(await screen.findByText(/Charmander/)).toBeInTheDocument()
    expect(await screen.findByText(/Charmeleon/)).toBeInTheDocument()
    expect(await screen.findByText(/Charizard/)).toBeInTheDocument()
    expect(await screen.findByText(/Squirtle/)).toBeInTheDocument()
    expect(await screen.findByText(/Wartortle/)).toBeInTheDocument()
    expect(await screen.findByText(/Blastoise/)).toBeInTheDocument()
    expect(await screen.findByText(/Caterpie/)).toBeInTheDocument()
})

test("renders pokemon list and checks the index", async () => {
    render(
        <MockedProvider mocks={mocks} addTypename={false}>
            <Pokedisplay />
        </MockedProvider>
    )

    expect((await screen.findByText(/# 1$/)).parentElement?.children[1]).toContainHTML("Bulbasaur")
    expect((await screen.findByText(/# 2$/)).parentElement?.children[1]).toContainHTML("Ivysaur")
    expect((await screen.findByText(/# 3$/)).parentElement?.children[1]).toContainHTML("Venusaur")
    expect((await screen.findByText(/# 4$/)).parentElement?.children[1]).toContainHTML("Charmander")
    expect((await screen.findByText(/# 5$/)).parentElement?.children[1]).toContainHTML("Charmeleon")
    expect((await screen.findByText(/# 6$/)).parentElement?.children[1]).toContainHTML("Charizard")
    expect((await screen.findByText(/# 7$/)).parentElement?.children[1]).toContainHTML("Squirtle")
    expect((await screen.findByText(/# 8$/)).parentElement?.children[1]).toContainHTML("Wartortle")
    expect((await screen.findByText(/# 9$/)).parentElement?.children[1]).toContainHTML("Blastoise")
    expect((await screen.findByText(/# 10$/)).parentElement?.children[1]).toContainHTML("Caterpie")

})



