import "@testing-library/jest-dom"
import { fireEvent, render, screen } from "@testing-library/react"
import { MockedProvider } from "@apollo/client/testing"
import getPokemonList from "../Api/queries/pokemon/getPokemonList"
import Searchbar from "../Components/Searchbar/Searchbar"
import Pokedisplay from "../Components/Pokedisplay/Pokedisplay"
import { createContext, ReactElement } from "react"

const mocks = [
    {
        request: {
            query: getPokemonList(["pokemon{pokeIndex, name, types, img}, count"]),
            variables: { pagenr: 1, name: "" },
        },
        result: {
            data: {
                pokemonList: {
                    count: 11,
                    pokemon: [
                        {
                            pokeIndex: 1,
                            name: "Bulbasaur",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/1.png",
                            types: ["grass", "poison"],
                        },
                        {
                            pokeIndex: 2,
                            name: "Ivysaur",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/2.png",
                            types: ["grass", "poison"],
                        },
                        {
                            pokeIndex: 3,
                            name: "Venusaur",

                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/3.png",
                            types: ["grass", "poison"],
                        },
                        {
                            pokeIndex: 4,
                            name: "Charmander",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/4.png",
                            types: ["fire"],
                        },
                        {
                            pokeIndex: 5,
                            name: "Charmeleon",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/5.png",
                            types: ["fire"],
                        },
                        {
                            pokeIndex: 6,
                            name: "Charizard",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/6.png",
                            types: ["fire", "flying"],
                        },
                        {
                            pokeIndex: 7,
                            name: "Squirtle",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/7.png",
                            types: ["water"],
                        },
                        {
                            pokeIndex: 8,
                            name: "Wartortle",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/8.png",
                            types: ["water"],
                        },
                        {
                            pokeIndex: 9,
                            name: "Blastoise",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/9.png",
                            types: ["water"],
                        },
                        {
                            pokeIndex: 10,
                            name: "Caterpie",
                            img: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/10.png",
                            types: ["bug"],
                        }
                    ]
                }
            }
        }
    }
]

test("renders searchbar", async () => {
    render(
        <MockedProvider mocks={mocks} addTypename={false}>
            <Searchbar />
        </MockedProvider>
    )
    const searchbar = await screen.findByTestId("searchbar")
    expect(searchbar).toBeInTheDocument()
})

test("input text into searchbar", async () => {
    render(
        <MockedProvider mocks={mocks} addTypename={false}>
            <Searchbar />
        </MockedProvider>
    )
    const searchbar = await screen.findByTestId("searchbar")
    const input:HTMLInputElement = await screen.findByTestId("input")
    expect(input).toBeInTheDocument()
    expect(input).toHaveValue("")
    input.value = "Bulbasaur"
    expect(input).toHaveValue("Bulbasaur")
})


