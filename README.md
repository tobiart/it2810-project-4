# Prosjekt 3

For prosjekt tre har vi valgt å lage en applikasjon som viser alle skapningene i spill-serien Pokemon (en pokedéx if you will). I ettekant ser vi at litt mye fokus ble lagt på å videreutvikle funksjonalitet, noe som førte til at noen av kravene ikke ble møtt tilstrekkelig.

## Data

Dataen for applikasjonen er da de 905 eksisterende pokemon-ene fra alle generasjonene av Pokemon spill. Hver pokemon er lagret som et Pokemon objekt som blant annet inneholder: index, navn, bilde, typer, osv. I tillegg lagres det data om brukere, eller "trainers", som åpner for brukergenerert data. 

### Søk og filtrering
Søk foregår igjennom et input felt hvor man kan søke på pokemon etter navn. Dersom man søker med navn brukes inputen som en substring, som vil si at man vil få tilbake en liste av pokemon som har navn som inneholder den substringen. Man kan også filtere listen med pokemon igjennom to nedtrekksmenyer der man kan velge hvilke typene til pokemon-ene man søker etter.

Applikasjonen gir mulighet for sortering pokemon ved index, navn, eller mengde av upvotes eller downvotes, både stigende eller synkende igjennom nedtrekksmenyer. Denne sorteringen foregår i backend ved uthenting fra databasen, slik at sorteringen foregår på hele datasettet, og sidetall viser sortert innhold i riktig rekkefølge.


### Presentasjon av data
Applikasjonen den store datamengden ved bruk av "paging". Et kall av getPokemonList henter bare 50 pokemon fra databasen av gangen, og når man blar sider i klienten sender man bare et kall om neste/forrige side.

Man kan se nærmere på en pokemon ved å klikke på en av "pokemon-boksene" som vil da åpne et Modal element som viser med detaljert info om pokemon-en, som typer, evolutions, og votes.

Bruker/trainer vil man kunne se i sidebaren på høyresiden etter man har skrevet inn et brukernavn. Sidebaren vil vise "teamet" av pokemon til den brukeren dersom man har lagt til noen pokemon til laget.


### Brukergenerert data
Det brukergenererte dataen er form av upvotes og downvotes som en bruker kan gi en av til hver pokemon dersom man er logget inn. Disse tallene kan sorteres etter, og man kan se det totale mengden upvotes og downvotes til en pokemon dersom man klikker på den. 
I tillegg kan en bruker velge å legge til opp til seks pokemon til laget sitt, som vil lagres i databasen og kunne hentes senere. 


## Universell utforming / bærekraftig utvikling
Vi tenker applikasjonen klarer seg greit i henhold til "forsåelighet", men mangler aspekter på andre områder, som mangel på alt-tekst for bilder, og mangel på tilgang til funskjonalitet kun igjennom tastatur som eksempler.

Når det gjelder bærekraftig utvikling har applikasjonen mange svakheter, men tar noen steg for å minimere energibruk. Det som bruker mest unødvendig energi på applikasjonen er nok mengden api-kall klienten gjør ved vanlig bruk, og dette forverres med mengden bilder som blir sendt (som blir 50 per side). Et lite plaster på såret er bruken av caching for å forhindre å repetere kall og derfor hindrer noe energibruk. 
Bruken av av caching forhindrer at unødvendig repetisjon av identiske kall, men det er kanskje bare et plaster på såret. 


## Teknologi
Backend kjører en Express server i Nodejs, og bruker en Mongodb database. I frontend har graphql api-kall blitt kjørt med Apollo. 


## Testing
Testing ble satt av litt for lenge, men vi endte opp med to små enhets-tester for Searchbar og PokeDisplay, og noe end2end testing i App.test.


