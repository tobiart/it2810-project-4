import express from 'express'
import cors from 'cors'
import { graphqlHTTP } from 'express-graphql'
import { buildSchema } from 'graphql'
import { MongoClient } from 'mongodb';


const client = new MongoClient('mongodb://admin:admin@129.241.104.157:27017', {
        auth: {
        username: 'admin',
        password: 'admin'
    }
});

var pokemonCollection = undefined;
var trainerCollection = undefined;
run()

async function run(){
    try {
        await client.connect();
        console.log("Connected correctly to server");
        const database = client.db("pokedex", {authSource: "admin"});
        pokemonCollection = database.collection("pokemon");
        trainerCollection = database.collection("trainer");
        // let allTrainers = await trainerCollection.find({}).toArray()
        // let trainerNames = allTrainers.map((trainer) => {
        //     return trainer.username
        // })
        // console.log(trainerNames);
        // trainerCollection.updateMany({}, {$set: {team: []}})
        // trainerCollection.updateMany({}, {$set: {upvotes: []}})
        // trainerCollection.updateMany({}, {$set: {downvotes: []}})
        // trainerCollection.updateOne({username: "testuser"}, {$set: {upvotes: []}})
    } catch (err) {
        console.log(err.stack);
    }
}



// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
    type Pokemon {
        pokeIndex: Int
        name: String
        nextEvo: [Int]
        prevEvo: [Int]
        img: String
        types: [String]
        upvotes: Int
        downvotes: Int
    }
    type PokemonList {
        pokemon: [Pokemon]
        count: Int
    }

    type Trainer {
        username: String
        team: [Pokemon]
        upvotes: [Pokemon]
        downvotes: [Pokemon]
    }

    type Query {
        pokemon(pokeIndex: Int, nextEvo:[Int]) : Pokemon
        pokemonList(pagenr: Int, prevEvo:[Int] sortBy: String, sortOrder: Int, name: String, type1: String, type2: String) : PokemonList
        trainer(username: String!) : Trainer

    }

    type Mutation {
        addTrainer(username: String!) : Trainer

        addPokemonToTeam(username: String!, pokeIndex: Int!) : Trainer
        removePokemonFromTeam(username: String!, pokeIndex: Int!) : Trainer

        addUpvote(pokeIndex: Int!, username: String!) : Pokemon
        removeUpvote(pokeIndex: Int!, username: String!) : Pokemon
        addDownvote(pokeIndex: Int!, username: String!) : Pokemon
        removeDownvote(pokeIndex: Int!, username: String!) : Pokemon
    }
`);

// Helper function to convert database list of pokemon indexes to list of pokemon objects
function trainerHelper(trainer){
    let trainerCopy = JSON.parse(JSON.stringify(trainer)); // Deep copy
    let trainerTeam = trainer.team.map((pokeIndex) => {
        return pokemonCollection.findOne({pokeIndex: pokeIndex})
    }) 
    let trainerUpvotes = trainer.upvotes.map((pokeIndex) => {
        return pokemonCollection.findOne({pokeIndex: pokeIndex})
    }) 
    let trainerDownvotes = trainer.downvotes.map((pokeIndex) => {
        return pokemonCollection.findOne({pokeIndex: pokeIndex})
    }) 
    trainerCopy.team = trainerTeam
    trainerCopy.upvotes = trainerUpvotes
    trainerCopy.downvotes = trainerDownvotes
    return trainerCopy
}


// The root provides a resolver function for each API endpoint
var root = {
    // ------------- QUERYs -------------
    pokemon: ({pokeIndex, nextEvo}) => {
        if (pokeIndex) {
            return pokemonCollection.findOne({pokeIndex: pokeIndex})
        } else if (nextEvo){
            return pokemonCollection.findOne({nextEvo: nextEvo})
        } else {
            return null
        }
    },

    pokemonList: ({pagenr, sortBy, sortOrder, name, type1, type2, prevEvo}) => {
        if (!pagenr){
            pagenr = 0
        } else {
            pagenr -= 1
        }
        if (!name){
            name = "";
        }
        if (!sortBy){
            sortBy = "pokeIndex"
        }
        if (!sortOrder){
            sortOrder = 1
        }
        let sortObj = {}
        if (sortBy == "upvotes" || sortBy == "downvotes"){
            sortObj[sortBy] = sortOrder
            sortObj["pokeIndex"] = 1
        } else {
            sortObj[sortBy] = sortOrder
        }

        if(prevEvo) {

            // return pokemonCollection.aggregate([
            //     {$match: {$and: [{name: {$regex: name, $options: "$i"}},{prevEvo: prevEvo}]}},
            //     {$facet: {
            //         count: [{$count: "count"}],
            //         pokemon: [{$sort: sortObj}, {$skip: pagenr*50}, {$limit: 50}]
            //     }
            // }]).toArray().then((result) => {
            //     let count = result[0].count[0].count
            //     console.log(count)
            //     let pokemon = result[0].pokemon
            //     return {pokemon: pokemon, count: count}
            // })


            return pokemonCollection.find({$and: [{name: {$regex: name, $options: "$i"}},{prevEvo: prevEvo}]}).sort(sortObj).skip(50*pagenr).limit(50).toArray().then((pokemon) => {
                return {pokemon: pokemon, count: pokemon.length}
            })
        };
        if (type2) {
            if (type1 === type2 || type1 === '') {

                return pokemonCollection.aggregate([
                    {$match: {$and: [{name: {$regex: name, $options: "$i"}},{types: [type1||type2]}]}},
                    {$facet: {
                        count: [{$count: "count"}],
                        pokemon: [{$sort: sortObj}, {$skip: pagenr*50}, {$limit: 50}]
                    }
                }]).toArray().then((result) => {
                    let count = result[0].count[0]?.count ?? 0
                    let pokemon = result[0].pokemon
                    return {pokemon: pokemon, count: count}
                })

                // return pokemonCollection.find({$and: [{name: {$regex: name, $options: "$i"}}, {types: [type1]}]}).sort(sortObj).skip(50*pagenr).limit(50).toArray().then((pokemon) => {
                //     return {pokemon: pokemon, count: 4};
                // });
            } else {

                return pokemonCollection.aggregate([
                    {$match: {$and: [{name: {$regex: name, $options: "$i"}},{types: {$all: [type1, type2]}}]}},
                    {$facet: {
                        count: [{$count: "count"}],
                        pokemon: [{$sort: sortObj}, {$skip: pagenr*50}, {$limit: 50}]
                    }
                }]).toArray().then((result) => {
                    let count = result[0].count[0]?.count ?? 0
                    let pokemon = result[0].pokemon
                    return {pokemon: pokemon, count: count}
                })

                // return pokemonCollection.find({$and: [{name: {$regex: name, $options: "$i"}}, {types: type1}, {types: type2}]}).sort(sortObj).skip(50*pagenr).limit(50).toArray().then((pokemon) => {
                //     return {pokemon: pokemon, count: 5};
                // });
            }
        } else if (type1) {

            return pokemonCollection.aggregate([
                {$match: {$and: [{name: {$regex: name, $options: "$i"}},{types: type1}]}},
                {$facet: {
                    count: [{$count: "count"}],
                    pokemon: [{$sort: sortObj}, {$skip: pagenr*50}, {$limit: 50}]
                }
            }]).toArray().then((result) => {
                let count = result[0].count[0]?.count ?? 0
                let pokemon = result[0].pokemon
                return {pokemon: pokemon, count: count}
            })


            // return pokemonCollection.find({$and: [{name: {$regex: name, $options: "$i"}},{types: type1}]}).sort(sortObj).skip(50*pagenr).limit(50).toArray().then((pokemon) => {
            //     return {pokemon: pokemon, count: 6};
            // });
        } else {

            return pokemonCollection.aggregate([
                {$match: {name: {$regex: name, $options: "$i"}}},
                {$facet: {
                    count: [{$count: "count"}],
                    pokemon: [{$sort: sortObj}, {$skip: pagenr*50}, {$limit: 50}]
                }
            }]).toArray().then((result) => {
                let count = result[0].count[0]?.count ?? 0
                let pokemon = result[0].pokemon
                return {pokemon: pokemon, count: count}
            })

            // return pokemonCollection.find({name: {$regex: name, $options: "$i"}}).sort(sortObj).skip(50*pagenr).limit(50).toArray().then((pokemon) => {
            //     return {pokemon: pokemon, count: 7};
            // });
        }
    },

    trainer: ({username}) => {
        if (username === "" || username === undefined || username === null) {
            return
        }
        return trainerCollection.findOne({username: username}).then((trainer) => {
            if (trainer) {
                return trainerHelper(trainer);
            } else {
                return
            }
        })
        
    },

    // ------------- MUTATIONs -------------

    addTrainer: ({username}) => {
        if (username === "" || username === undefined || username === null) {
            return
        }
        return trainerCollection.findOne({username: username}).then((trainer) => {
            if (trainer) {
                return trainer;
            } else {
                return trainerCollection.insertOne({username: username, team: [], upvotes: [], downvotes: []}).then(() => {
                    return trainerCollection.findOne({username: username});
                });
            }
        });
    },

    addPokemonToTeam: ({username, pokeIndex}) => {
        return trainerCollection.findOne({username: username}).then((trainer) => {
            if (trainer) {
                if (trainer.team.length < 6) {
                    return pokemonCollection.findOne({pokeIndex: pokeIndex}).then((pokemon) => {
                        if (pokemon) {
                            return trainerCollection.updateOne({username: username}, {$push: {team: pokeIndex}}).then(() => {
                                return trainerCollection.findOne({username: username}).then((updatedTrainer) => {
                                    if (trainer) {
                                        return trainerHelper(updatedTrainer);
                                    } else {
                                        return trainerHelper(trainer);
                                    }
                                })
                            });
                        } else {
                            return trainerHelper(trainer);
                        }
                    });
                } else {
                    return trainerHelper(trainer);
                }
            } else {
                return trainerHelper(trainer);
            }
        });
    },

    removePokemonFromTeam: ({username, pokeIndex}) => {
        return trainerCollection.findOne({username: username}).then((trainer) => {
            if (trainer) {
                return pokemonCollection.findOne({pokeIndex: pokeIndex}).then((pokemon) => {
                    if (pokemon) {
                        for (var i = 0; i < trainer.team.length; i++) {
                            if (trainer.team[i] === pokeIndex) {
                                trainer.team.splice(i, 1);
                                break;
                            }
                        }

                        return trainerCollection.updateOne({username: username}, {$set: {team: trainer.team}}).then(() => {
                            return trainerCollection.findOne({username: username}).then((updatedTrainer) => {
                                if (updatedTrainer) {
                                    return trainerHelper(updatedTrainer);
                                } else {
                                    return trainerHelper(trainer);
                                }
                            });
                        });
                        
                    } else {
                        return trainerHelper(trainer);
                    }
                });
            } else {
                return trainerHelper(trainer);
            }
        });
    },

    addUpvote: ({pokeIndex, username}) => {
        return pokemonCollection.findOne({pokeIndex: pokeIndex}).then((pokemon) => {
            if (pokemon) {
                return trainerCollection.findOne({username: username}).then((trainer) => {
                    if (trainer) {
                        var alreadyUpvoted = false;
                        trainer.upvotes.map((upvotedPokemon) => {
                            if (upvotedPokemon === pokemon.pokeIndex) {
                                alreadyUpvoted = true;
                            }
                        });
                        if (alreadyUpvoted) {
                            return pokemon;
                        } else {
                            return pokemonCollection.findOneAndUpdate({pokeIndex: pokeIndex}, {$inc: {upvotes: 1}}).then(() => {
                                return pokemonCollection.findOne({pokeIndex: pokeIndex}).then((updatedPokemon) => {
                                    return trainerCollection.findOneAndUpdate({username: username}, {$push: {upvotes: pokeIndex}}).then(() => {
                                        return updatedPokemon;
                                    });
                                });
                            });
                        }
                    } else {
                        return pokemon;
                    }
                });
            } else {
                return pokemonCollection.findOne({name: "amoonguss"});
            }
        });
    },
    
    removeUpvote: ({pokeIndex, username}) => {
        if (pokemon.upvotes === 0){
            return pokemon;
        }
        return pokemonCollection.findOne({pokeIndex: pokeIndex}).then((pokemon) => {
            if (pokemon) {
                return trainerCollection.findOne({username: username}).then((trainer) => {
                    if (trainer) {
                        var alreadyUpvoted = false;
                        trainer.upvotes.map((upvotedPokemon) => {
                            if (upvotedPokemon === pokemon.pokeIndex) {
                                alreadyUpvoted = true;
                            }
                        });
                        if (alreadyUpvoted) {
                            return pokemonCollection.findOneAndUpdate({pokeIndex: pokeIndex}, {$inc: {upvotes: -1}}).then(() => {
                                return trainerCollection.findOneAndUpdate({username: username}, {$pull: {upvotes: pokeIndex}}).then(() => {
                                    return pokemonCollection.findOne({pokeIndex: pokeIndex})
                                });
                            });
                        } else {
                            return pokemon;
                        }
                    } else {
                        return pokemon;
                    }
                });
            } else {
                return pokemonCollection.findOne({name: "amoonguss"});
            }
        });
    },

    addDownvote: ({pokeIndex, username}) => {
        return pokemonCollection.findOne({pokeIndex: pokeIndex}).then((pokemon) => {
            if (pokemon) {
                return trainerCollection.findOne({username: username}).then((trainer) => {
                    if (trainer) {
                        var alreadyDownvoted = false;
                        trainer.downvotes.map((downvotedPokemon) => {
                            if (downvotedPokemon === pokemon.pokeIndex) {
                                alreadyDownvoted = true;
                            }
                        });
                        if (alreadyDownvoted) {
                            return pokemon;
                        } else {
                            return pokemonCollection.findOneAndUpdate({pokeIndex: pokeIndex}, {$inc: {downvotes: 1}}).then(() => {
                                return pokemonCollection.findOne({pokeIndex: pokeIndex}).then((updatedPokemon) => {
                                    return trainerCollection.findOneAndUpdate({username: username}, {$push: {downvotes: pokeIndex}}).then(() => {
                                        return updatedPokemon;
                                    });
                                });
                            });
                        }
                    } else {
                        return pokemon;
                    }
                });
            } else {
                return pokemonCollection.findOne({name: "amoonguss"});
            }
        });
    },

    removeDownvote: ({pokeIndex, username}) => {
        return pokemonCollection.findOne({pokeIndex: pokeIndex}).then((pokemon) => {
            if (pokemon.downvotes === 0){
                return pokemon;
            }
            if (pokemon) {
                return trainerCollection.findOne({username: username}).then((trainer) => {
                    if (trainer) {
                        var alreadyDownvoted = false;
                        trainer.downvotes.map((downvotedPokemon) => {
                            if (downvotedPokemon === pokemon.pokeIndex) {
                                alreadyDownvoted = true;
                            }
                        });
                        if (alreadyDownvoted) {
                            return pokemonCollection.findOneAndUpdate({pokeIndex: pokeIndex}, {$inc: {downvotes: -1}}).then(() => {
                                return trainerCollection.findOneAndUpdate({username: username}, {$pull: {downvotes: pokeIndex}}).then(() => {
                                    return pokemonCollection.findOne({pokeIndex: pokeIndex})
                                });
                            });
                        } else {
                            return pokemon;
                        }
                    } else {
                        return pokemon;
                    }
                });
            } else {
                return pokemonCollection.findOne({name: "amoonguss"});
            }
        });
    }

};

var app = express();
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
}));
var server = app.listen(4000);
console.log('Running a GraphQL API server at localhost:4000/graphql');
server.on('close', () => {
    client.close();
});
