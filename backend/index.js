// const { MongoClient, ServerApiVersion } = require('mongodb');
// const fs = require('fs');
import { MongoClient, ServerApiVersion } from 'mongodb';
import fs from 'fs';
import fetch from 'node-fetch';



const credentials = 'X509-cert-228500106462723285.pem'

const client = new MongoClient('mongodb+srv://pokeurmom.mv5clur.mongodb.net/?authSource=%24external&authMechanism=MONGODB-X509&retryWrites=true&w=majority', {
  sslKey: credentials,
  sslCert: credentials,
  serverApi: ServerApiVersion.v1
});

var pokemon = [];

async function run() {
  try {
    await client.connect();
    const database = client.db("pokedex"); 
    const collection = database.collection("pokemon");

    // Gets a list of all pokemon and shows their names and next evolution

    // var pokemon = await collection.find({}).toArray();
    // for (var i = 0; i < pokemon.length; i++) {
    //     if (pokemon[i]['next-evo']) {
    //         try {
    //             var evolutions = pokemon[i]['next-evo']
    //             if (evolutions.length > 1) {
    //                 evolutions = evolutions.map(evo => {return pokemon[evo]['name']})
    //                 console.log(pokemon[i]['name'] + ' evolves into ' + evolutions.join(', '));
    //             } else{
    //                 console.log(pokemon[i]['name'] + ' evolves into ' + pokemon[evolutions]['name']);
    //             }
    //         } catch (error) {
    //             console.log(error)
    //             console.log(pokemon[i]['name'] + " evolves into " + pokemon[i]['next-evo']);
    //         }
    //     }   else{
    //         console.log(pokemon[i]['name']);
    //     }
    // }

    // Gets data from the pokeapi and generates a new pokemon object

    // console.log("Fetching data from PokeAPI...");
    // for (let i = 1; i <= speciesNum; i++) {
    //     var pokemonJSON = {};
    //     var species = await fetch("https://pokeapi.co/api/v2/pokemon-species/" + i).then(response => response.json()).catch(error =>{
    //         console.log("Error fetching species " + i);
    //         console.log("Retrying...");
    //         i--;
    //     });
    //     if (!species) {
    //         continue;
    //     }
    //     pokemonJSON["poke-index"] = species.id;
    //     pokemonJSON["name"] = species.name;
    //     pokemonJSON["next-evo"] = [];
    //     pokemonJSON["prev-evo"] = [];
    //     pokemonJSON["img"] = species.name + ".png"
    //     var detailsURL;
    //     for (let j = 0; j < species.varieties.length; j++) {
    //         if (species.varieties[j].is_default) {
    //             detailsURL = species.varieties[j].pokemon.url;
    //         }
    //     }
    //     if (!detailsURL) {
    //         detailsURL = species.varieties[0].pokemon.url;
    //     }
    //     pokemonJSON["types"] = [];
    //     var details = await fetch(detailsURL).then(response => response.json()).catch(error => {
    //         console.log("Error fetching details " + i);
    //         console.log("Retrying...");
    //         i--;
    //     });
    //     if (!details) {
    //         continue;
    //     }
    //     for (let j = 0; j < details.types.length; j++) {
    //         pokemonJSON["types"].push(details.types[j].type.name);
    //     }
    //     pokemonJSON["upvotes"] = 0;
    //     pokemonJSON["downvotes"] = 0;
    //     if (species.evolves_from_species) {
    //         var prevEvo = await fetch(species.evolves_from_species.url).then(response => response.json()).catch(error => {
    //             console.log("Error fetching previous evolution " + i);
    //             console.log("Retrying...");
    //             i--;
    //         });
    //         if (!prevEvo) {
    //             continue;
    //         }
    //         pokemonJSON["prev-evo"].push(prevEvo.id);
    //     }
    //     console.log("Progress:", i, "/", speciesNum);
    //     pokemon.push(pokemonJSON);
    // }
    // console.log("Finished fetching data from PokeAPI!");

    // Reads the pokemon data from a json file

    // console.log("Reading data from JSON file...");
    // pokemon = JSON.parse(fs.readFileSync('pokemon.json', 'utf8'));
    // console.log("Finished reading data from JSON file!");

    // Fixes evolution data

    // console.log("Fixing evolution data...");
    // for (let i = 0; i < pokemon.length; i++) {
    //     if (pokemon[i]["prev-evo"].length > 0) {
    //         var prev = pokemon.find(poke => pokemon[i]["prev-evo"].includes(poke["poke-index"]));
    //         if (prev) {
    //             prev["next-evo"].push(pokemon[i]["poke-index"]);
    //             console.log("Assigned", pokemon[i]["name"], "as the evolution of", prev["name"]);
    //         } else {
    //             console.log("Failed to assign evolutiion");
    //             console.log("Tried to assign " + pokemon[i]["poke-index"] + " as the next evolution of " + pokemon[i]["prev-evo"]);
    //         }
    //     }
    // }
    // for (let i = 0; i < pokemon.length; i++) {
    //     if (pokemon[i]["next-evo"].length == 0) {
    //         pokemon[i]["next-evo"] = null;
    //     }
    //     if (pokemon[i]["prev-evo"].length == 0) {
    //         pokemon[i]["prev-evo"] = null;
    //     }
    // }
    // console.log("Finished fixing evolution data!");

    // Writes the pokemon data to a json file

    // console.log("Writing to file...");
    // fs.writeFile("pokemon.json", JSON.stringify(pokemon), function(err) {
    //     if (err) {
    //         return console.log(err);
    //     }
    //     console.log("Finished writing to file!");
    // });

    // Inserts the pokemon data into the database
    // console.log("Inserting data into database...");
    // const result = await collection.insertMany(pokemon);
    // console.log("Finished inserting data into database!");
    // console.log(`${result.insertedCount} documents were inserted`);

    
    // console.log(docCount);
    // perform actions using client
  } finally {
    // Ensures that the client will close when you finish/error
    await client.close();
  }
}

run().catch(console.dir);
